Updates [calibre](https://calibre-ebook.com/) on Mac.

- checks current version
- scrapes calibre's website to find the latest version
- downloads the img file if there's an update and installs the new version

Usage:

- Ensure you have Go installed
- run ```go get bitbucket.org/rsperl/updatecalibre```
- from ```$GOPATH/src/bitbucket.org/rsperl/updatecalibre``` run ```make```
- copy the binary ```updatecalibre``` somewhere convenient and run it