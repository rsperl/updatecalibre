package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"regexp"
	"strings"
)

var (
	home        string
	tmpdir      string
	downloadURL string
	versionURL  string
	installDir  string
)

func init() {
	home = os.Getenv("HOME")
	tmpdir = home + "/tmp"
	versionURL = "http://calibre-ebook.com/whats-new"
	downloadURL = "http://status.calibre-ebook.com/dist/osx32"
	installDir = home + "/Applications"
}

func getLatestVersion() string {
	client := &http.Client{}

	req, err := http.NewRequest("GET", versionURL, nil)

	resp, err := client.Do(req)

	if err != nil {
		fmt.Println("Failure : ", err)
	}

	respBody, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()

	var versionRe = regexp.MustCompile(`Release: (.+?) `)
	for _, line := range strings.Split(string(respBody), "\n") {
		// <h2 class="release-title">Release: 1.21 [24 Jan, 2014]</h2>
		if strings.Contains(line, "class=\"release-title\"") {
			resultSlice := versionRe.FindStringSubmatch(line)
			if len(resultSlice) > 1 {
				return resultSlice[1]
			}
		}
	}

	fmt.Println("failed to get the latest version")
	os.Exit(1)

	return ""
}

func getCurrentVersion() string {
	cmd := exec.Command("calibredb", "--version")
	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Run()
	str := out.String()

	// calibredb (calibre 2.49)
	re := regexp.MustCompile(`calibre (.+?)\)`)
	match := re.FindStringSubmatch(str)
	if len(match) > 1 {
		return match[1]
	}
	fmt.Println("failed to get current version")
	return ""
}

func download() string {
	client := &http.Client{}
	req, err := http.NewRequest("GET", downloadURL, nil)
	resp, err := client.Do(req)
	if err != nil {
		fmt.Printf("error downloading new version: %v\n", err)
		os.Exit(1)
	}
	respBody, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	filename := tmpdir + "/calibre.img"
	fh, _ := os.Create(filename)
	err = binary.Write(fh, binary.LittleEndian, respBody)
	return filename
}

func install(imgfile string) {
	var out bytes.Buffer
	var errstr bytes.Buffer

	fmt.Printf("mounting %s\n", imgfile)
	cmd := exec.Command("hdiutil", "attach", imgfile)
	err := cmd.Run()
	if err != nil {
		fmt.Printf("failed to mount %s\n", imgfile)
		os.Exit(1)
	}

	fmt.Println("removing old version")
	cmd = exec.Command("rm", "-rf", installDir+"/calibre.app")
	cmd.Run()

	fmt.Println("finding mount path")
	cmd = exec.Command("find", "/Volumes", "-maxdepth", "1", "-name", "*calib*")
	cmd.Stdout = &out
	cmd.Stderr = &errstr
	err = cmd.Run()
	if err != nil {
		fmt.Printf("failed to run command to get mount path: %v\n", err)
		fmt.Printf(errstr.String())
		os.Exit(1)
	}
	mountPath := out.String()
	mountPath = strings.Trim(mountPath, "\n")
	fmt.Printf("mount path is %s\n", mountPath)
	if mountPath == "" {
		fmt.Printf("failed to get mount path\n")
		os.Exit(1)
	}

	src := mountPath + "/calibre.app"
	fmt.Printf("copying calibre from %s to %s\n", src, installDir)
	cmd = exec.Command("cp", "-r", src, installDir)
	cmd.Run()

	fmt.Printf("unmounting %s\n", mountPath)
	cmd = exec.Command("hdiutil", "eject", mountPath)
	cmd.Run()

	fmt.Printf("removing %s\n", imgfile)
	cmd = exec.Command("rm", "-f", "imgfile")
	cmd.Run()
}

func main() {
	cv := getCurrentVersion()
	fmt.Printf("Current version: %s\n", cv)
	lv := getLatestVersion()
	fmt.Printf("Latest version:  %s\n", lv)

	if lv == cv {
		os.Exit(0)
	}

	file := download()
	install(file)
}
